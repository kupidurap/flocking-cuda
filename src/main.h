#ifndef MAIN_H
#define MAIN_H

#include <cuda.h>

#include <GL/glew.h>
#include "paramgl.h"

#include <stdlib.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>
#include "glslUtility.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "kernel.h"

#include <helper_cuda.h>
#include <helper_cuda_gl.h>
#define compat_getMaxGflopsDeviceId() gpuGetMaxGflopsDeviceId()

using namespace std;

GLuint positionLocation = 0;
GLuint velocityLocation = 1;

const char *planetAttributeLocations[] = {"Position", "Color", "Velocity"};

GLuint velocityVBO = (GLuint)NULL;
GLuint positionVBO = (GLuint)NULL;

GLuint program;

int width=1000; int height=1000;

int main(int argc, char** argv);

bool isPaused = false;
bool recall = false;
vec3 target = vec3(0,0,0);
int mouse_x = 0;
int mouse_y = 0;
int mouse_buttons = 0;
int mouse_pressed = 0;

float maxAcc = 3.0f;
float maxSpeed = 2.0f;

float separationNeighborhood = 10.0f;
float cohesionNeighborhood = 30.0f;
float alignmentNeighborhood = 10.0f;
float mouseNeighborhood = 5.0f;

float separationWeight = 2.5f;
float cohesionWeight = 1.0f;
float alignmentWeight = 1.5f;
float mouseWeight = 2.0f;

ParamListGL *params;
SimParams simParams;

bool displaySliders = false;

void runCuda();

void display();
void keyboard(unsigned char key, int x, int y);
void motion(int x, int y);
void mouse(int button, int state, int x, int y);
void special(int k, int x, int y);

void updateParams();
void initParams();

void init(int argc, char* argv[]);
void initCuda();
void initVAO();
void initShaders(GLuint& program);

void cleanupCuda();
void shut_down(int return_code);

#endif
