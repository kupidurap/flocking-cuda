#include "main.h"
#define DT 0.1

int numberOfBoids = 100;
struct cudaGraphicsResource* resources[2];

int main(int argc, char** argv)
{
    init(argc, argv);

    cudaGLSetGLDevice( compat_getMaxGflopsDeviceId() );


    cudaGraphicsGLRegisterBuffer(&resources[0],
                                 positionVBO,
                                 cudaGraphicsMapFlagsNone);
    cudaGraphicsGLRegisterBuffer(&resources[1],
                                 velocityVBO,
                                 cudaGraphicsMapFlagsNone);

	initParams();
	updateParams();
    initCuda(numberOfBoids);

    initShaders(program);

    glEnable(GL_DEPTH_TEST);

    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutSpecialFunc(special);
    glutMainLoop();

    return 0;
}

void runCuda()
{
	float time;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);

    float *dptrvert = NULL;
	float *velptr=NULL;
    size_t num_bytes;

    cudaGraphicsMapResources(2, resources, 0);

    cudaGraphicsResourceGetMappedPointer((void**)&dptrvert,
                                         &num_bytes,
                                         resources[0]);

    cudaGraphicsResourceGetMappedPointer((void**)&velptr,
                                         &num_bytes,
                                         resources[1]);

    vec3 mouse = vec3(mouse_x, mouse_y, mouse_pressed);

    cudaFlockingUpdate(DT, target, recall, mouse);
    cudaUpdateVBO(dptrvert, velptr);

    cudaGraphicsUnmapResources(2, resources, 0);
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
}

int timebase = 0;
int frame = 0;

void display()
{
    static float fps = 0;
    frame++;
    int time=glutGet(GLUT_ELAPSED_TIME);

    if (time - timebase > 1000) {
        fps = frame*1000.0f/(time-timebase);
        timebase = time;
        frame = 0;
    }

    updateParams();

	if (!isPaused)
		runCuda();

    char title[100];
    sprintf( title, "Flocking [%0.2f fps]", fps );
    glutSetWindowTitle(title);

    glClearColor(0.25, 0.25f, 0.25f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(program);

    glEnableVertexAttribArray(positionLocation);

    glBindBuffer(GL_ARRAY_BUFFER, positionVBO);
    glVertexAttribPointer((GLuint)positionLocation, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(velocityLocation);

	glBindBuffer(GL_ARRAY_BUFFER, velocityVBO);
	glVertexAttribPointer((GLuint)velocityLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);


	glDrawArrays(GL_POINTS, 0, numberOfBoids);

    glDisableVertexAttribArray(positionLocation);

    glUseProgram(0);

    if (displaySliders)
    {
        glDisable(GL_DEPTH_TEST);
        glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ZERO);
        glEnable(GL_BLEND);
        params->Render(0, 0);
        glDisable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
    }

    glutPostRedisplay();
    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case(27):
            exit(1);
            break;
		case 'p':
		case 'P':
			isPaused = !isPaused;
			break;
		case 'r':
		case 'R':
			recall = !recall;
			break;
        case 'h':
            displaySliders = !displaySliders;
            break;
    }
}

void mouse(int button, int state, int x, int y)
{
    if (state == GLUT_DOWN)
    {
        mouse_buttons |= 1<<button;
        mouse_pressed = 1;
    }
    else if (state == GLUT_UP)
    {
        mouse_buttons = 0;
        mouse_pressed = 0;
    }

    mouse_x = x;
    mouse_y = y;

    if (displaySliders)
	{
		if (params->Mouse(x, y, button, state))
		{
			glutPostRedisplay();
			return;
		}
	}
}

void motion(int x, int y)
{
    if (mouse_buttons & 1)
    {
        mouse_x = x;
        mouse_y = y;
    }

    if (displaySliders)
	{
		if (params->Motion(x, y))
		{
			glutPostRedisplay();
			return;
		}
	}
}

void special(int k, int x, int y)
{
    if (displaySliders)
    {
        params->Special(k, x, y);
    }
}

void init(int argc, char* argv[])
{
	numberOfBoids = atoi(argv[1]);

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(width, height);
    glutCreateWindow("Flocking simulation");

    glewInit();
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        std::cout << "glewInit failed, aborting." << std::endl;
        exit (1);
    }

    initVAO();
}

void initVAO(void)
{
    GLfloat *bodies    = new GLfloat[4*(numberOfBoids)];
	GLfloat *velocities = new GLfloat[3*(numberOfBoids)];

    for(int i = 0; i < numberOfBoids; i++)
    {
        bodies[4*i+0] = 0.0f;
        bodies[4*i+1] = 0.0f;
        bodies[4*i+2] = 0.0f;
        bodies[4*i+3] = 1.0f;

		velocities[3*i+0] = 0.0f;
		velocities[3*i+1] = 0.0f;
		velocities[3*i+2] = 0.0f;
    }

	glGenBuffers(1, &positionVBO);
	glGenBuffers(1, &velocityVBO);

    glBindBuffer(GL_ARRAY_BUFFER, positionVBO);
    glBufferData(GL_ARRAY_BUFFER, 4*(numberOfBoids)*sizeof(GLfloat), bodies, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, velocityVBO);
	glBufferData(GL_ARRAY_BUFFER, 3*(numberOfBoids)*sizeof(GLfloat), velocities, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    delete[] bodies;
	delete[] velocities;
}

void initParams()
{
	// create a new parameter list
	params = new ParamListGL("misc");

	params->AddParam(new Param<float>("Max speed", maxSpeed, 0.0f, 10.0f, 0.2f, &maxSpeed));
	params->AddParam(new Param<float>("Max acc", maxAcc, 0.0f, 10.0f, 0.2f, &maxAcc));

	params->AddParam(new Param<float>("Separation neighborhood", separationNeighborhood, 0.0f, 100.0f, 1.0f, &separationNeighborhood));
	params->AddParam(new Param<float>("Alignment neighborhood"  , alignmentNeighborhood , 0.0f, 100.0f, 1.0f, &alignmentNeighborhood));
	params->AddParam(new Param<float>("Cohesion neighborhood"  , cohesionNeighborhood , 0.0f, 100.0f, 1.0f, &cohesionNeighborhood));
	params->AddParam(new Param<float>("Mouse neighborhood"  , mouseNeighborhood , 0.0f, 100.0f, 1.0f, &mouseNeighborhood));

	params->AddParam(new Param<float>("Separation weight", separationWeight, 0.0f, 10.0f, 0.2f, &separationWeight));
	params->AddParam(new Param<float>("Alignment weight", alignmentWeight, 0.0f, 10.0f, 0.2f, &alignmentWeight));
	params->AddParam(new Param<float>("Cohesion weight", cohesionWeight, 0.0f, 10.0f, 0.2f, &cohesionWeight));
	params->AddParam(new Param<float>("Mouse weight", mouseWeight, 0.0f, 10.0f, 0.2f, &mouseWeight));
}

void updateParams() {
	simParams.alignmentNeighborhood = alignmentNeighborhood;
	simParams.alignmentWeight = alignmentWeight;
	simParams.cohesionNeighborhood = cohesionNeighborhood;
	simParams.cohesionWeight = cohesionWeight;
	simParams.separationNeighborhood = separationNeighborhood;
	simParams.separationWeight = separationWeight;
	simParams.mouseNeighborhood = mouseNeighborhood;
	simParams.mouseWeight = mouseWeight;
	simParams.maxAcc = maxAcc;
	simParams.maxSpeed = maxSpeed;

	setParameters(&simParams);
}

void initShaders(GLuint& program)
{
    program = glslUtility::createProgram("shaders/boidVS.glsl", "shaders/boidGS.glsl", "shaders/boidFS.glsl", planetAttributeLocations, 3);
    glUseProgram(program);
}

void shut_down(int return_code)
{
    exit(return_code);
}
