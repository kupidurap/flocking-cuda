#ifndef KERNEL_H
#define KERNEL_H

#include <stdio.h>
#include <thrust/random.h>
#include <cuda.h>
#include <cmath>

#include <helper_math.h>

#define blockSize 128
#define checkCUDAErrorWithLine(msg) checkCUDAError(msg, __LINE__)

using glm::vec3;
using glm::vec4;
using glm::length;
using glm::normalize;

typedef unsigned int uint;

// simulation parameters
struct SimParams
{
	float maxAcc;
	float maxSpeed;

	float separationNeighborhood;
	float cohesionNeighborhood;
	float alignmentNeighborhood;
	float mouseNeighborhood;

	float separationWeight;
	float cohesionWeight;
	float alignmentWeight;
	float mouseWeight;
};

void setParameters(SimParams *hostParams);
void checkCUDAError(const char *msg, int line);
void cudaFlockingUpdate(float dt, vec3 target, bool recall, vec3 mouse);
void initCuda(int N);
void cudaUpdateVBO(float * vbodptr, float *velptr);
#endif
