#include <stdio.h>
#include <cuda.h>
#include <cmath>
#include "glm/glm.hpp"
#include "kernel.h"

dim3 threadsPerBlock(blockSize);

int numObjects;
const float scene_scale = 2e2;

__constant__ SimParams params;

const __device__ float min_x = -100.0f;
const __device__ float max_x = 100.0f;
const __device__ float min_y = -100.0f;
const __device__ float max_y = 100.0f;

vec4 * dev_pos;
vec3 * dev_vel;

void checkCUDAError(const char *msg, int line = -1)
{
    cudaError_t err = cudaGetLastError();
    if( cudaSuccess != err)
    {
        if( line >= 0 )
        {
            fprintf(stderr, "Line %d: ", line);
        }
        fprintf(stderr, "Cuda error: %s: %s.\n", msg, cudaGetErrorString( err) );
        exit(EXIT_FAILURE);
    }
}

__host__ __device__
unsigned int hash(unsigned int a){
    a = (a+0x7ed55d16) + (a<<12);
    a = (a^0xc761c23c) ^ (a>>19);
    a = (a+0x165667b1) + (a<<5);
    a = (a+0xd3a2646c) ^ (a<<9);
    a = (a+0xfd7046c5) + (a<<3);
    a = (a^0xb55a4f09) ^ (a>>16);
    return a;
}

__host__ __device__
vec3 generateRandomNumberFromThread(float time, int index)
{
    thrust::default_random_engine rng(hash(index*time));
    thrust::uniform_real_distribution<float> u01(0,1);

    return vec3((float) u01(rng), (float) u01(rng), (float) u01(rng));
}

__global__
void generateRandomPosArray(int time, int N, vec4 * arr, float scale)
{
    int index = (blockIdx.x * blockDim.x) + threadIdx.x;
    if(index < N)
    {
        vec3 rand = scale*(generateRandomNumberFromThread(time, index)-0.5f);
        arr[index].x = rand.x;
        arr[index].y = rand.y;
        arr[index].z = 0.0f;
        arr[index].w = 1.0f;
    }
}

__global__
void initializeVelArray(int time, int N, vec3 *vel)
{
    int index = (blockIdx.x * blockDim.x) + threadIdx.x;
    if(index < N)
    {
    	vec3 rand = (generateRandomNumberFromThread(time, index));
		vel[index] = vec3(rand.x,rand.y,0);
    }
}

__device__
vec3 integrateAcceleration(vec3 vel, vec3 acc, float dt, int N, vec4 mypos, vec4 *positions)
{
	vec3 nextVel = vec3(0,0,0);
	nextVel = vel + acc * dt;
	return nextVel;
}

__device__
vec3 integrateVelocity(vec3 position, vec3 velocity, float dt)
{
	vec3 nextPosition = vec3(0,0,0);
	nextPosition = position + velocity * dt;
	return nextPosition;
}

__device__
vec3 arrival(vec4 my_pos, vec3 target)
{
	vec3 arrivalDirection = target - vec3(my_pos);
	float distToTarget = length(arrivalDirection);
	float threshold = 10.0;

	if (distToTarget < threshold)
		return vec3(0,0,0);
	else
	{
		return arrivalDirection * params.maxSpeed;
	}
}

__device__
float magnitude(vec3 vector)
{
	return sqrt(vector.x * vector.x + vector.y * vector.y);
}

__device__
void normalize(vec3& vector)
{
	float m = magnitude(vector);
	if (m <= 0) return;
	vector /= m;
}

__device__
void limit(vec3& vector, double max)
{
	float m = magnitude(vector);
	if (m <= max) return;
	normalize(vector);
	vector *= max;
}

__device__
float eucl_dist(vec3 first, vec3 second)
{
	float x = first.x - second.x;
	float y = first.y - second.y;
	return sqrt(x * x + y * y);
}

__device__
vec3 flock(int N, vec4 my_pos, vec4* boids_pos, vec3* boids_vel)
{
	vec3 desiredVelocity = vec3(0,0,0);
	vec3 separateDirection = vec3(0,0,0);
	vec3 alignmentDirection = vec3(0,0,0);
	vec3 cohesionDirection = vec3(0,0,0);

	int separationCount = 0;
	int numAlignedBoids = 0;
	int numCohesiveBoids = 0;

	for (int i = 0 ; i < N ; ++i)
	{
		if (my_pos == boids_pos[i])
			continue;

		vec3 toBoidI = vec3(my_pos) - vec3(boids_pos[i]);
		float toBoidILen = eucl_dist(vec3(my_pos), vec3(boids_pos[i]));
		if (toBoidILen < params.separationNeighborhood && toBoidILen > 0)
		{
			normalize(toBoidI);
			separateDirection += (10.0f * toBoidI) / (toBoidILen * toBoidILen + (float)1e-12);
			separationCount++;
		}

		if (toBoidILen < params.alignmentNeighborhood && toBoidILen > 0)
		{
			alignmentDirection += boids_vel[i];
			numAlignedBoids++;
		}

		if (toBoidILen < params.cohesionNeighborhood && toBoidILen > 0)
		{
			cohesionDirection += vec3(boids_pos[i]);
			numCohesiveBoids++;
		}

	}

	if (numAlignedBoids > 0)
	{
		alignmentDirection = alignmentDirection / (float)numAlignedBoids;

		float len = length(alignmentDirection) + 1e-6;
		alignmentDirection = 10.0f * alignmentDirection / len;
	}

	if (numCohesiveBoids > 0)
	{
		cohesionDirection = cohesionDirection / (float)numCohesiveBoids;
		cohesionDirection = cohesionDirection - vec3(my_pos.x, my_pos.y, my_pos.z);
	}

	desiredVelocity = params.separationWeight * separateDirection +
					  params.cohesionWeight * cohesionDirection+
					  params.alignmentWeight * alignmentDirection;

	return desiredVelocity;
}

__device__
void wrapBoid(vec3 &pos)
{
	if (pos.x < min_x)
		pos.x = max_x;
	else if (pos.x > max_x)
		pos.x = min_x;

	if (pos.y < min_y)
		pos.y = max_y;
	else if (pos.y > max_y)
		pos.y = min_y;
}

__global__
void updateVelocity(int N, float dt, vec4 * pos, vec3 * vel, vec3 target, bool recall, vec3 mouse_pos)
{
    int index = threadIdx.x + (blockIdx.x * blockDim.x);
    if( index < N )
    {
    	bool mousePressed = mouse_pos.z == 1;
        vec4 my_pos = pos[index];
		vec3 my_vel = vel[index];

		if (!recall)
		{
			vec3 flockingVelocity;

			flockingVelocity = flock(N, my_pos, pos, vel);

			if (mousePressed) {
				mouse_pos.z = 0.0f;
				vec3 mouse = mouse_pos - vec3(my_pos);
				float d = length(mouse) - params.mouseNeighborhood;

				if (d < 0) d = 0.01;

				if (d > 0 && d < params.mouseNeighborhood) {
					normalize(mouse);
					mouse /= (d * d);
					mouse *= -1;
					mouse *= params.mouseWeight;
					flockingVelocity += mouse;
				}
			}

			float flockingVelocityMagnitude = length(flockingVelocity) + 1e-10;
			float myVelocityMagnitude = length(my_vel);
			vec3 flockingDirection = flockingVelocity / flockingVelocityMagnitude;

			float accMag = ((flockingVelocityMagnitude - myVelocityMagnitude) / dt);
			vec3 acc = fminf(accMag, params.maxAcc) * flockingDirection;

			vel[index] = integrateAcceleration(vel[index], 0.15f * acc, dt, N, my_pos, pos);

			float newVelMag = length(vel[index]) + 1e-10;
			vec3 newVelDirection = vel[index] / newVelMag;
			vel[index] = fminf(newVelMag, params.maxSpeed) * newVelDirection;
		}
		else
		{
			vel[index] = integrateAcceleration(vel[index], 0.15f * (arrival(my_pos, target)-my_vel)/dt, dt, N, my_pos, pos);
		}
    }
}

__global__
void updatePosition(int N, float dt, vec4 *pos, vec3 *vel)
{
	int index = threadIdx.x + (blockIdx.x * blockDim.x);
	if (index < N )
	{
		vec3 nextPosition = integrateVelocity(vec3(pos[index]), vel[index], dt);
		wrapBoid(nextPosition);

		pos[index].x = nextPosition.x;
		pos[index].y = nextPosition.y;
		pos[index].z = 0.0f;
	}
}

__global__
void sendToVBO(int N, vec4 * pos, vec3 * vel, float * vbo, float *velVBO, float s_scale)
{
    int index = threadIdx.x + (blockIdx.x * blockDim.x);

    float c_scale_w = -2.0f / s_scale;
    float c_scale_h = -2.0f / s_scale;

    if(index<N)
    {
        vbo[4*index+0] = pos[index].x*c_scale_w;
        vbo[4*index+1] = pos[index].y*c_scale_h;
		vbo[4*index+2] = 0.0f;
        vbo[4*index+3] = 1;

		velVBO[3*index+0] = vel[index].x*c_scale_w;
		velVBO[3*index+1] = vel[index].y*c_scale_h;
		velVBO[3*index+2] = 0.0f;
    }
}

/*************************************
 * Wrappers for the __global__ calls *
 *************************************/

//Initialize memory, update some globals
void initCuda(int N)
{
    numObjects = N;
    dim3 fullBlocksPerGrid((int)ceil(float(N)/float(blockSize)));

    cudaMalloc((void**)&dev_pos, N*sizeof(vec4));
    checkCUDAErrorWithLine("Kernel failed!");
    cudaMalloc((void**)&dev_vel, N*sizeof(vec3));
    checkCUDAErrorWithLine("Kernel failed!");

    generateRandomPosArray<<<fullBlocksPerGrid, blockSize>>>(1, numObjects, dev_pos, scene_scale);
	cudaThreadSynchronize();
    checkCUDAErrorWithLine("Kernel failed!");
    initializeVelArray<<<fullBlocksPerGrid, blockSize>>>(2, numObjects, dev_vel);
    checkCUDAErrorWithLine("Kernel failed!");
	cudaThreadSynchronize();
}

void setParameters(SimParams *hostParams)
{
    // copy parameters to constant memory
    cudaMemcpyToSymbol(params, hostParams, sizeof(SimParams));
    checkCUDAErrorWithLine("Setting parameters error!");
}

void cudaFlockingUpdate(float dt, vec3 target, bool recall, vec3 mouse)
{
	checkCUDAErrorWithLine("Kernel failed!");
    dim3 fullBlocksPerGrid((int)ceil(float(numObjects)/float(blockSize)));
    float z = mouse.z;
    mouse /= 5;
	mouse -= 100;
	mouse.x *= -1;
	mouse.z = z;
    updateVelocity<<<fullBlocksPerGrid, blockSize, blockSize*sizeof(vec4)>>>(numObjects, dt, dev_pos, dev_vel, target, recall, mouse);
	checkCUDAErrorWithLine("Kernel failed!");
	updatePosition<<<fullBlocksPerGrid, blockSize>>>(numObjects, dt, dev_pos, dev_vel);
    checkCUDAErrorWithLine("Kernel failed!");
	cudaThreadSynchronize();
}

void cudaUpdateVBO(float * vbodptr, float *velptr)
{
    dim3 fullBlocksPerGrid((int)ceil(float(numObjects)/float(blockSize)));
    sendToVBO<<<fullBlocksPerGrid, blockSize>>>(numObjects, dev_pos, dev_vel, vbodptr, velptr, scene_scale);
    checkCUDAErrorWithLine("Kernel failed!");
	cudaThreadSynchronize();
}
