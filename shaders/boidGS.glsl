#version 330

layout(points) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3[] gVelocity;

void main() {
	vec3 vel = gVelocity[0];
	vec3 pep = vec3(-vel.y, vel.x, 0.0f);
	pep = normalize(pep);
	pep = pep * 0.005;
	
	vel = normalize(vel);
	vel = vel * 0.02;
		
    gl_Position = gl_in[0].gl_Position + vec4(pep, 0.0f);
    EmitVertex();
    
    gl_Position = gl_in[0].gl_Position - vec4(pep, 0.0f);
    EmitVertex();

    gl_Position = gl_in[0].gl_Position + vec4(vel, 0.0f);
    EmitVertex();

    EndPrimitive();
}