#version 330

in vec4 Position;
in vec3 velocity;

out vec3 gVelocity;

void main(void)
{
	gl_Position = Position;
	gVelocity = velocity;
}