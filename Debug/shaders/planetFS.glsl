#version 330

in fColor
{
	vec3 color;
}frag;

in vec3 WorldCoord;
in vec3 ToCam;
in vec3 Up;
in vec3 Right;
in vec2 TexCoord;
out vec4 FragColor;

void main()
{
	FragColor = vec4(frag.color, 1.0);
}