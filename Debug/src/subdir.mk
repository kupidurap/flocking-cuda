################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/glslUtility.cpp \
../src/utilities.cpp 

CU_SRCS += \
../src/FlockingSimulation.cu \
../src/kernel.cu 

CU_DEPS += \
./src/FlockingSimulation.d \
./src/kernel.d 

OBJS += \
./src/FlockingSimulation.o \
./src/glslUtility.o \
./src/kernel.o \
./src/utilities.o 

CPP_DEPS += \
./src/glslUtility.d \
./src/utilities.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cu
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/opt/cuda/bin/nvcc -I/usr/local/cuda/samples/common -I/usr/local/cuda/samples/common/inc -I/opt/cuda/samples/common/inc -I/home/samba/kupidurap/cuda-workspace/FlockingSimulation/src -I/home/samba/kupidurap/cuda-workspace/FlockingSimulation/glm -G -g -O0 -gencode arch=compute_20,code=sm_20  -odir "src" -M -o "$(@:%.o=%.d)" "$<"
	/opt/cuda/bin/nvcc -I/usr/local/cuda/samples/common -I/usr/local/cuda/samples/common/inc -I/opt/cuda/samples/common/inc -I/home/samba/kupidurap/cuda-workspace/FlockingSimulation/src -I/home/samba/kupidurap/cuda-workspace/FlockingSimulation/glm -G -g -O0 --compile --relocatable-device-code=false -gencode arch=compute_20,code=compute_20 -gencode arch=compute_20,code=sm_20  -x cu -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/opt/cuda/bin/nvcc -I/usr/local/cuda/samples/common -I/usr/local/cuda/samples/common/inc -I/opt/cuda/samples/common/inc -I/home/samba/kupidurap/cuda-workspace/FlockingSimulation/src -I/home/samba/kupidurap/cuda-workspace/FlockingSimulation/glm -G -g -O0 -gencode arch=compute_20,code=sm_20  -odir "src" -M -o "$(@:%.o=%.d)" "$<"
	/opt/cuda/bin/nvcc -I/usr/local/cuda/samples/common -I/usr/local/cuda/samples/common/inc -I/opt/cuda/samples/common/inc -I/home/samba/kupidurap/cuda-workspace/FlockingSimulation/src -I/home/samba/kupidurap/cuda-workspace/FlockingSimulation/glm -G -g -O0 --compile  -x c++ -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


